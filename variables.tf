locals {
  name       = "gke-tf"
  project_id = "channyeinkoko"
  region     = "australia-southeast1"

  public_subnet = "public-ause1"
  subnetwork    = "gke-tf-ause1"

  ip_ranges = {
    "pod" = {
      name  = "ip-range-pods"
      range = "192.168.0.0/18"
    },
    "service" = {
      name  = "ip-range-svc"
      range = "172.16.0.0/18"
    }
  }
}

data "google_client_config" "default" {}
