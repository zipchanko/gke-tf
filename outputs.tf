output "gcp_network" {
  value = module.gcp-network
}

output "cloud_nat" {
  value = module.cloud-nat
}
