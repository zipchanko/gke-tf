module "gke" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster-update-variant"
  version = "~> 31.0"

  project_id                 = local.project_id
  name                       = "${local.name}-cluster"
  regional                   = true
  region                     = local.region
  network                    = module.gcp-network.network_name
  kubernetes_version         = "1.28"
  subnetwork                 = local.subnetwork
  ip_range_pods              = local.ip_ranges.pod.name
  ip_range_services          = local.ip_ranges.service.name
  create_service_account     = true
  enable_private_nodes       = true
  master_ipv4_cidr_block     = "172.64.0.0/28"
  remove_default_node_pool   = true
  deletion_protection        = false
  add_cluster_firewall_rules = true
  release_channel            = "UNSPECIFIED"

  node_pools = [
    {
      name         = "pool-01"
      min_count    = 1
      max_count    = 5
      machine_type = "e2-small"
      auto_upgrade = false
      spot         = true
    },
  ]

  node_pools_metadata = {
    pool-01 = {
      shutdown-script = "kubectl --kubeconfig=/var/lib/kubelet/kubeconfig drain --force=true --ignore-daemonsets=true --delete-local-data \"$HOSTNAME\""
    }
  }
}
