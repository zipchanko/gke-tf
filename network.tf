module "gcp-network" {
  source  = "terraform-google-modules/network/google"
  version = "~> 7.0"

  project_id   = local.project_id
  network_name = local.name
  routing_mode = "REGIONAL"

  subnets = [
    {
      subnet_name           = local.subnetwork
      subnet_ip             = "10.0.0.0/17"
      subnet_region         = local.region
      subnet_private_access = "true"
    },
  ]

  secondary_ranges = {
    (local.subnetwork) = [
      {
        range_name    = local.ip_ranges.pod.name
        ip_cidr_range = local.ip_ranges.pod.range
      },
      {
        range_name    = local.ip_ranges.service.name
        ip_cidr_range = local.ip_ranges.service.range
      },
    ]
  }

  routes = [{
    name              = "egress-internet"
    description       = "route through IGW to access internet"
    destination_range = "0.0.0.0/0"
    tags              = "egress-inet"
    next_hop_internet = "true"
  }]

  egress_rules = [{
    name               = "${local.name}-egress-internet"
    destination_ranges = ["0.0.0.0/0"]

    allow = [{
      protocol = "tcp"
      ports    = null
      }, {
      protocol = "udp"
      ports    = null
    }]
  }]

  ingress_rules = [{
    name               = "${local.name}-ingress-ssh"
    source_ranges      = ["10.0.0.0/8"]
    destination_ranges = ["10.0.0.0/8"]

    allow = [{
      ports    = ["22"]
      protocol = "tcp"
    }]
  }]
}

module "cloud-nat" {
  source  = "terraform-google-modules/cloud-nat/google"
  version = "~> 5.0"

  project_id                         = local.project_id
  region                             = local.region
  create_router                      = true
  router                             = "router-ause1"
  network                            = module.gcp-network.network_name
  name                               = "nat-ause1"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}
