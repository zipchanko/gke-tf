terraform {
  cloud {
    organization = "keiko23"

    workspaces {
      name = "gke-tf"
    }
  }

  required_providers {
    google = {
      source = "hashicorp/google"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
  }
  required_version = ">= 0.13"
}

provider "google" {
  project = local.project_id
  region  = local.region

  default_labels = {
    Terraform = "true"
  }
}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}
